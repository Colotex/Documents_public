# 公司整體現代化 (Modernize Colotex)

> 公司網址: https://www.colotex.net/

目標: 其目的使公司更趨近於現代標準

## ❓ 問題

1. 網頁設計過於老舊 2009 (距今2023, 已經過14年). 現今網路時代, 人們已經不在透過親自拜
訪到其客戶的公司來其看門面. 現在的時代是透過網站, 和名片上的網址來識別一間公司的是否還健
在. 在外國人的認知中, 如果一個網站太舊或沒有更新, 會大大的降低它們的出單意願. 他們可能
會害怕這間公司會不會直接倒閉. 間著看看理想的網站設計, https://www.polartec.com/.
2. Logo 太複雜. 對於 B2C (Business-to-consumer) 的公司來說, 太過於複雜的 Logo
比較不好讓人記住, 宣傳效果或許會不太好.

| Logo 1 | Logo 2 |
|---|---|
| ![](etc/logo.png)| <img src="./etc/colotex_logo.gif" width="30%"/> |

到底哪個才是公司 Logo?

### 🕵️ 已知網站缺陷

#### 設計方面

1. 在 `Aboutus` 頁面, 是一張圖片. 當網頁式圖片的時候, 是無法增加搜尋引擎的知名度.
簡單來說, 上網查詢我們公司的時候會被排在比較後面.
2. 錯字太多 `Aboutus` -> `About Us`, `Contactus` -> `Contact Us`, 等等.
3. 網站應該要多元, 並足以展示我們公司的產品印象. 例如: 
  - POLARTEC它們的頁面有拍攝它們工廠的製作流程(片段而已), 會讓人感到安心, 並且保有
  資訊透明 (這點很重要, 因為顧客喜歡被騙, 如果有疑慮, 也會造成下單意願降低).

![trans](etc/trans.png)

#### 資訊方面

1. 沒有展現優勢. 例如, 我們已經跟那些廠商合作過. 這些經驗可以當作 [Partners](https://www.polartec.com/news?Category=Partners)
頁面來展現我們是個可靠的廠商, 並且非常有經驗. 下面舉例:

```
曾與 New Balance 達成合作關係 從 20xx 年至 20xx 年! (點進去可以寫比較詳細的說明, 也可以選擇不做內頁)
```

2. News 如果沒有定期更新的話, 會讓人以為我們沒做了. 如果沒有持續訊更新,
可以考慮直接拿掉.
3. 介紹字體盡量保持**清晰**和**整潔**. 把重點標出來, 有需要客人會再點進去看詳細的說明:

![clean](etc/clear.png)

## 🔍 商標 (Trademark) vs 標註 (Logo)

> A trademark protects a slogan, phrase, word, company name, logo, or design
> that identifies a company and/or its goods. A logo is a symbol or design
> used by a company that may fall under trademark protection laws.

如果當初註冊是 Colotex, 那 Colotex 就是我們的Logo? 如果不是, 可以選擇重新塑造
一個簡單易懂的 Logo. 現代 Logo 都是選擇簡單就好, 例如 `POLARTEC` 的三個三角形,
Nike 的勾勾, 等等. 主目的是讓人好記住他, 這也是宣傳手段之一.

太複雜的 Logo 不容易讓人記住, 如果產品不是**特別出色**, 那就更難宣傳.
